import React from 'react';

const Button = (props) => {
    const btnCss=`btn btn-success ${props.typebtn} ${props.css}`
    return (
        <button className={btnCss} onClick={props.click}>{props.children}</button>
    );
}

export default Button;
