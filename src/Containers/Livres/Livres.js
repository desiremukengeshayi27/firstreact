import React, { Component } from 'react';
import Livre from './Livre/Livre';
import FormulaireAjout from '../FormulaireAjout/FormulaireAjout';
import FormulaireModification from '../FrolumaireModification.js/FormulaireModification';

class Livres extends Component {
  state = {
    livres:[
        {id:1,titre:"l'algorithme selon moi", auteur:"desire Muk", nbpages:400},
        {id:2,titre:"La france du 19 eme", auteur:"ALbert patrick", nbpages:500},
        {id:3,titre:"Le monde des animaux", auteur:"Marc merlin", nbpages:250},
        {id:4,titre:"le virus d'assis", auteur:"Tya ben", nbpages:120},
    ],
    lastIdLivre : 4,
    idLivreAModifier : 0,
  }

  handleSuppressionLivre = (id) =>{
    const livreIndexTab= this.state.livres.findIndex(l=>{
        return l.id === id;
    })

    const newLivres = [...this.state.livres];
    newLivres.splice(livreIndexTab,id)

    this.setState({livres:newLivres})
  }

  handleAjoutLivre = (titre, auteur, nbpages) =>{
    const newLivre={
        id:this.state.lastIdLivre + 1,
        titre:titre,
        auteur:auteur,
        nbpages:nbpages,
    };
    const newListeLivres=[...this.state.livres];
    newListeLivres.push(newLivre);

    this.setState(oldState => {
       return {
        livres: newListeLivres,
        lastIdLivre: oldState.lastIdLivre + 1,
       }
    })
    this.props.fermerAjoutLivre();
  }


  render() {
    return (
        <>
            <table class="table table-striped text-center">
                <thead>
                <tr className='table-info'>
                    <th>Titre</th>
                    <th>Auteur</th>
                    <th>Nombre de pages</th>
                    <th colSpan="2">Actions</th>
                </tr>
                </thead>
                <tbody>
                    {
                        this.state.livres.map(livre=>{
                            if(livre.id !== this.state.idLivreAModifier){
                                return (
                                    <tr key={livre.id}>
                                        <Livre 
                                            titre={livre.titre}
                                            auteur={livre.auteur}
                                            nbpages={livre.nbpages}
                                            suppression={()=> this.handleSuppressionLivre(livre.id)}
                                            modification={()=> this.setState({idLivreAModifier:livre.id})}

                                        />
                                    </tr>
                                );
                            }else{
                                return <FormulaireModification  titre={livre.titre}
                                            auteur={livre.auteur}
                                            nbpages={livre.nbpages}/> 
                            }
                        })
                    }
                
                </tbody>
            </table> 
            {this.props.ajoutLivre && <FormulaireAjout validation= {this.handleAjoutLivre} />}
        </>
    );
}
}
export default Livres
