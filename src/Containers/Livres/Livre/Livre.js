import React from 'react';
import Button from '../../../Components/Button/Button';

const Livre = (props) => ( 
    <>
        <td>{props.titre} </td>
        <td>{props.auteur} </td>
        <td>{props.nbpages} </td>
        <td><Button typebtn='btn-warning' click={props.modification}>Modifier</Button></td>
        <td><Button typebtn='btn-danger' click={props.suppression}>Supprimer</Button></td>
    </>
)

export default Livre;
