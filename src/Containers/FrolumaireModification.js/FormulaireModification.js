import React, { Component } from 'react'
import Button from '../../Components/Button/Button'

export default class FormulaireModification extends Component {
    state={
        titresaisi:this.props.titre,
        auteurSaisi:this.props.auteur,
        nbPageSaisi:this.props.nbpages, 
    }
    handleValidationFor=()=>{
        console.log('modifier')
    }
  render() {
    return (
      <>
            <td> 
                <input type="text" 
                    value={this.state.titresaisi} 
                    onChange={(event)=>this.setState({titresaisi:event.target.value})}
                  />
            </td>
            <td> 
                <input type="text" 
                  value={this.state.auteurSaisi} 
                  onChange={(event)=>this.setState({auteurSaisi:event.target.value})}

                  />
            </td>
            <td> 
                <input type="number"  
                  id="nbrePage" 
                  value={this.state.nbPageSaisi} 
                  onChange={(event)=>this.setState({nbPageSaisi:event.target.value})}
                 />
            </td>
            <td><Button typebtn='btn-primary' click={this.handleValidationForm}>Valider</Button></td>
      </>
    )
  }
}

