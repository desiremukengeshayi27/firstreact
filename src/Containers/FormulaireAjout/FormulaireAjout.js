import React, { Component } from 'react'
import Button from '../../Components/Button/Button';

class FormulaireAjout extends Component {
  state={
    titresaisi:"",
    auteurSaisi:"",
    nbPageSaisi:"", 
  }
  handleValidationForm =(e)=>{
    e.preventDefault();
    this.props.validation(this.state.titresaisi,this.state.auteurSaisi,this.state.nbPageSaisi)
    this.setState({
      titresaisi:"",
      auteurSaisi:"",
      nbPageSaisi:"", 
    })
  }

  render() {
    return (
      <>
        <h1 className='text-center text-primary'>Affichage de Formulaire d'ajout</h1>
        <form>
            <div class="form-group mt-4"> 
                <label htmlfor="titre" className="form-label">Titre du livre</label>
                <input type="text" 
                  className="form-control" 
                  id="titre" 
                  value={this.state.titresaisi} 
                  onChange={(event)=>this.setState({titresaisi:event.target.value})}/>
            </div>
            <div class="form-group mt-4"> 
                <label htmlfor="auteur" className="form-label">Auteur</label>
                <input type="text" 
                  className="form-control" id="auteur" 
                  value={this.state.auteurSaisi} 
                  onChange={(event)=>this.setState({auteurSaisi:event.target.value})}/>
            </div>
            <div class="form-group mt-4"> 
                <label htmlfor="nbrePage" className="form-label">Nombre de pages</label>
                <input type="number" 
                  className="form-control" 
                  id="nbrePage" 
                  value={this.state.nbPageSaisi} 
                  onChange={(event)=>this.setState({nbPageSaisi:event.target.value})}/>
            </div>
            <Button typebtn='btn-primary' click={this.handleValidationForm}>Valider</Button>
        </form>
      </>
          
    );
  };
}
export default FormulaireAjout;