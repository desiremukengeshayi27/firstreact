import React, { Component } from 'react';
import Titre from './Components/Titre/Titre'
import Button from './Components/Button/Button'
import Livres from './Containers/Livres/Livres';

class App extends Component {
  state={
    ajoutLivre:false,
  }
  
  handleAjoutLivre = () => {
    this.setState((oldState, props)=>{
       return{ajoutLivre: !oldState.ajoutLivre}
    })
  }

  render() {
    return (
      <div className='container'>
        <Titre>Page Listant les livres</Titre>
        <Livres ajoutLivre={this.state.ajoutLivre} fermerAjoutLivre={()=> this.setState({ajoutLivre:false})} ></Livres>
        <Button 
          typebtn='btn-success' 
          css='w-100' 
          click={()=>this.handleAjoutLivre()}>
        {!this.state.ajoutLivre? "Ajouter" : "Fermer Ajout"}
        </Button>
    </div>
  
    )
  }
}
export default App;
